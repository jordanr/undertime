.\"                                      Hey, EMACS: -*- nroff -*-
.\" (C) Copyright 2018 anarcat anarcat@debian.org,
.\"
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH UNDERTIME 1 "June 2020"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.mso  www.tmac
.SH NAME
undertime \- pick a meeting time
.SH SYNOPSIS
.\" do not forget to update the UndertimeArgumentParser and completion
 \" in extra/ when you change anything below
.B undertime
.RB [\| OPTIONS \]
.RB [\| \-\-timezones
.IR ZONES \|]
.IR [WHEN]
.br
.B undertime
.RB [ \-l | \-\-list\-zones ]
.RB [ \-V | \-\-version ]
.br
.B undertime
.RI \-\-selftest
.\" do not forget to update the UndertimeArgumentParser and completion
 \" in extra/ when you change anything above
.SH DESCRIPTION
.PP
.\" TeX users may be more comfortable with the \fB<whatever>\fP and
.\" \fI<whatever>\fP escape sequences to invoke boldface and italics,
.\" respectively.
\fBundertime\fP is a program that allows you to quickly pick a meeting
time across multiple time zones for conference calls or other
coordinated events. It shows all times of a given day for all the
time zones selected, in a table that aligns the time so a line shows
simultaneous times across all time zones. This takes into account
daylight savings and other peculiarities (provided that the local
time zone database is up to date) so you can also schedule meetings in
the future as well.
.PP
An educated guess is made to find the relevant time zones: the local
time zone is first guessed and added to the list, then the arguments on
the command line are looked up in the internal time zone
database. Spaces in arguments are replaced by underscores and all-caps
versions of lowercase strings are tried as well. When failing to match
a provided time zone against the internal list, a warning is emitted. A
complete list of time zones is shown when the \fB\-\-list\-zones\fP
command line option is provided.
.PP
You can also specify offsets instead of time zones, with the syntax
\fIUTC{+|-}offset\fP or \fIGMT{+|-}offset\fP (for example UTC+2 for
CEST or GMT-4 for EDT). Those are discouraged as daylight savings are
not taken into account so the computed time might not represent the
expected local time. Don't confuse those patterns with the "standard"
time zones \fIEtc/GMT{+|-}X\fP: they are the inverse of each other
(i.e. \fIEDT\fP, \fIUTC-4\fP is \fIEtc/GMT+4\fP, yes, that's
confusing). To specify the Etc time zones the full time zone needs to
given (for example \fIEtc/GMT-2\fP). Those should also be avoided:
just use normal, location-based zones instead of those arbitrary
offsets.
.PP
Colors are used to highlight the "work hours" where possible meeting
times could overlap and the current time is marked in bold. Colors are
replaced by an underscore suffix and the current time by a star if
colors are disabled. You can change those work hours with the
\fB\-\-start\fP and \fB\-\-end\fP flags.
.PP
Because daylight savings may actually change time, you should also
pick an arbitrary time for the meeting (\fIWHEN\fP), using natural
language (as parsed by the parsedatetime library). The current time is
also shown, in bold.
.SH OPTIONS
.\" do not forget to change the SYNOPSIS above when you change
.\" anything below
.TP
.BI \-s\   HOUR \fR,\ \fB\-\-start\  HOUR
start of working day, in hours
.TP
.BI \-e\   HOUR \fR,\ \fB\-\-end\  HOUR
end of working day, in hours
.TP
.B \-\-no\-colors, \-\-colors
do not show colors. enabled by default if output is a terminal.
.TP
.BI \fB\-\-timezones\  ZONES
target date for the meeting, supports arbitrary dates like "in two
weeks"
.TP
.B \-\-no\-default-zone, \-\-default\-zone
do not show current time zone first
.TP
.B \-\-no-unique, \-\-unique
deduplicate time zones
.TP
.B \-\-no\-overlap, \-\-overlap
do not show the overlap column (labeled "n"), displayed by default.
.TP
.BI \-\-overlap\-min\   N
show only times with N overlapping time zones.
.TP
.BI \-\-format\   FORMAT
pass \fIFORMAT\fP to the tabulate package to format the main table.
.TP
.BI \-\-config\   FILE
load YAML config \fIFILE\fP as defaults.
.TP
.B \-v, \-\-verbose
enable verbose messages
.TP
.B \-\-debug
enable debugging messages
.TP
.B \-l, \-\-list\-zones
show valid time zones and exit
.TP
.B \-V, \-\-version
show version number and exit
.TP
.B \-\-selftest
run test suite
.TP
.B \-h, \-\-help
show summary of options and defaults
.\" do not forget to change the SYNOPSIS above when you change
.\" anything above
.SH ENVIRONMENT
.TP
.B TZ
Default time zone.
.TP
.B NO_COLOR
If this environment variable is defined, no color will be shown unless
the \-\-colors flag is passed. Comforming to
.URL https://no-color.org/ "NO_COLOR informal standard"
.SH FILES
.TP
.I ~/.config/undertime.yml $XDG_CONFIG_DIR/undertime.yml
The configuration file loaded by default, if present. The
configuration file is formatted as a YAML dictionary, where keys are
the long option names (without dashes) and the values are their
values. Comments start with the \fI#\fR character. Options provided as
arguments override the defaults set in the configuration file,
particularly time zones. For example, this means time zones specified in
the configuration file are ignored ignored when also specified as
arguments. The \fB\-\-config\fR parameter allows you to load another
configuration file, on top of the default one, using the same logic,
allowing you to have multiple "presets".
.SH EXAMPLE
.SS Command line options
.TP
\fBundertime \fB--timezones \fI"New York" "Los Angeles" Paris
Display possible meeting times for three major cities.
.TP
\fBundertime -s \fI8 \fB-e \fI18 \fI2014-03-15
Show a longer work day in march 2014.
.TP
\fBundertime \fB--overlap-min \fI3 \fI2014-03-15
Show only times that match in three time zones.
.TP
\fBundertime \fB--timezones \fI"New York" "Los Angeles" Paris -- tomorrow
Show times for those three timezones, but for tomorrow. Note the
options separator (\-\-) is required otherwise the date will be parsed
as a timezone.
.SS Configuration file
A sample configuration file should be available in
.I /usr/share/doc/undertime/examples/undertime.yml
.SH SEE ALSO
.BR tzselect (8)
.BR tzfile (5)
.br
.URL https://en.wikipedia.org/wiki/List_of_tz_database_time_zones "Time zones list"
.br
.URL https://pypi.python.org/pypi/parsedatetime/ "parsedatetime Python package"
.br
